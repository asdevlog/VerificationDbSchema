﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace Plugin_MSSQL
{
    public class Plugin_MSSQL : Plugin
    {
        public string PluginName()
        {
            return "MSSQL Plugin";
        }

        public DataTable ResulDT(string serverName,string databaseName)
        {
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader myReader;
            DataTable dataTable = new DataTable();
            cn.ConnectionString = "Server = " + serverName + "; Database = " + databaseName + "; integrated Security = SSPI;";
            cn.Open();

            cmd.Connection = cn;
            cmd.CommandText = " SELECT DB_NAME() + isc.Table_Name + Column_Name as id," +
                                     "  DB_NAME() AS DBName," +
                                     "  isc.Table_Name AS TableName ," +
                                     "  isc.Table_Schema AS SchemaName ," +
                                     "  Ordinal_Position AS  Ord ," +
                                     "   Column_Name ," +
                                     "   Data_Type ," +
                                     "   Numeric_Precision AS  Prec ," +
                                     "   Numeric_Scale AS  Scale ," +
                                     "   Character_Maximum_Length AS LEN ," +
                                     "  Is_Nullable," +
                                     "  Column_Default," +
                                     "  Table_Type " +
                                     "  FROM     INFORMATION_SCHEMA.COLUMNS isc " +
                                     "  INNER JOIN  information_schema.tables ist " +
                                     "              ON isc.table_name = ist.table_name " +
                                     " ORDER BY DBName,TableName,SchemaName,Ordinal_position;";

            myReader = cmd.ExecuteReader(CommandBehavior.KeyInfo);


            if (myReader.HasRows)
            {
                dataTable.Load(myReader);
            }
            myReader.Close();
            cn.Close();

            return dataTable;

        }
    }
}
