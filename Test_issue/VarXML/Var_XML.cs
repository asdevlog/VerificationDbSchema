﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace VarXML
{
    public enum dbType
    {
        MSSQL,
        MYSQL,
    }

    public class Var_XML
    {
      
        public string resultString { get; set; }
        public string errorString { get; set; }
        public DataTable resultDT { get; set; }

        /// <summary>
        /// Функция производит выгрузку эталонной структуры БД в XML файл
        /// </summary>
        /// <param name="databaseName">Имя базы данных</param>
        /// <param name="fileName">Файл назначения куда быдет выгруженна эталонная структура</param>
        /// <param name="BaseType">Тип БД</param>
        /// <param name="serverName">Сервер БД( по умолчанию localhost)</param>
        public void SchemaToXML(string databaseName,string fileName, string serverName = "localhost", DataTable outDT = null)
        {
            if (!VerificationInputParametr(databaseName, fileName))
            {
                return;
            }
  
            DataTable DT = new DataTable();
            try
            {
                if (outDT == null)
                    DT = GetSchema(databaseName, serverName);
                else
                    DT = outDT;
            }
            catch (Exception e)
            {
                errorString = e.Message.ToString();
                resultString = "Ошибка подключения к БД";
                return;
            }
            DT.TableName = "T1";
                DT.WriteXml(fileName + ".xml");
                resultString = "Выгрузка произведена успешно.";
            
            

        }

        /// <summary>
        /// Функция производит сравнение эталлонного файла с физической структурой БД
        /// </summary>
        /// <param name="databaseName">Имя базы данных</param>
        /// <param name="fileName">Файл с эталонной структурой</param>
        /// <param name="BaseType">Тип БД</param>
        /// <param name="serverName">Сервер БД( по умолчанию localhost)</param>
        public void Verification_dbSchema(string databaseName, string fileName, string serverName = "localhost", DataTable out_DT = null)
        {
            if (!VerificationInputParametr(databaseName, fileName))
            {                
                return;
            }

            if (!File.Exists(fileName + ".xml"))
            {
                resultString = "Файл не найден.";
                return;

            }
            DataTable DT_TARGET = new DataTable();
            DataTable DT_TARGET_OUT = new DataTable("T1");
            DataTable DT_SOURCE = new DataTable("T1");
            bool error = false;
            try
            { if (out_DT == null)
                    DT_TARGET = GetSchema(databaseName, serverName);
                else
                    DT_TARGET = out_DT;
            }
            catch (Exception e)
            {
                resultString = "Ошибка подключения к БД";
                errorString = e.Message.ToString();
                return;
            }
            
            DT_TARGET.TableName = "T1";
            DT_TARGET.WriteXml("tmp.xml");
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            DT_TARGET_OUT.ReadXmlSchema("tmp.xml");
            DT_TARGET_OUT.ReadXml("tmp.xml");
            //Используем такой прием для по причине того что при чтении DT из XML тип всех полей становиться string
            DT_SOURCE.ReadXmlSchema(fileName + ".xml");
            DT_SOURCE.ReadXml(fileName + ".xml");
           
            //Находим различия в структуре БД по сравнению с эталонной xml
            var differentROW_ST = DT_SOURCE.AsEnumerable().Except(DT_TARGET_OUT.AsEnumerable(), DataRowComparer.Default);
            //Находим различия которые есть в структуре бд но нет эталонной xml
            var differentROW_TS = DT_TARGET_OUT.AsEnumerable().Except(DT_SOURCE.AsEnumerable(), DataRowComparer.Default);

            Detail_Verification_dbSchema(DT_SOURCE, DT_TARGET_OUT);
            

            string fileName_log = "log.txt";
            FileStream aFile = new FileStream(fileName_log, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(aFile);

            sw.WriteLine("Изменения структуры бд по сравнению с эталонной структурой:");
            sw.WriteLine("Нет данных(или данные изменены) в структуре БД по следующим полям:");
            

            foreach (DataRow row in differentROW_ST)
              {
                error = true;               
                sw.WriteLine("SOURCE | {0} | {1} | {2} | {3}",
                      row["Ord"], row["TableName"], row["Column_Name"], row["Data_Type"]);
              }
            sw.WriteLine("Нет данных(или данные изменены в БД) в эталонной структуре по следующим полям:");
            foreach (DataRow row in differentROW_TS)
            {
                error = true;
                sw.WriteLine("TARGET | {0} | {1} | {2} | {3}",
                      row["Ord"], row["TableName"], row["Column_Name"], row["Data_Type"]);
            }

            sw.Close();
            aFile.Close();
            if (error)
            {
                DataTable d1 = differentROW_ST.CopyToDataTable();
                DataTable d2 = differentROW_TS.CopyToDataTable();

                d1.Merge(d2);
                resultDT = d1;
                resultDT.TableName = "ResultError";
                resultDT.WriteXml("MIN_error_log.xml");
                resultString += "Обнаружены ошибки в структуре. Для подробностей смотрите файл логов";
            }
            else
            {
                resultString += "Ошибок в структуре не обнаружено";
            }
            


        }

        /// <summary>
        /// Функция возвращает структуру БД в формате DataTable
        /// </summary>
        /// <param name="databaseName">Имя базы данных</param>
        /// <param name="BaseType">ТипБазыДанных</param>
        /// <param name="serverName">Имя сервера</param>
        /// <returns></returns>
        public DataTable GetSchema(string databaseName, string serverName = "localhost")
        {
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader myReader;
            DataTable dataTable = new DataTable();
            cn.ConnectionString = "Server = " + serverName + "; Database = " + databaseName + "; integrated Security = SSPI;";
            cn.Open();
                       
            cmd.Connection = cn;
            cmd.CommandText = " SELECT DB_NAME() + isc.Table_Name + Column_Name as id," +
                                     "  DB_NAME() AS DBName," +
                                     "  isc.Table_Name AS TableName ," +
                                     "  isc.Table_Schema AS SchemaName ," +
                                     "  Ordinal_Position AS  Ord ," +
                                     "   Column_Name ," +
                                     "   Data_Type ," +
                                     "   Numeric_Precision AS  Prec ," +
                                     "   Numeric_Scale AS  Scale ," +
                                     "   Character_Maximum_Length AS LEN ," +
                                     "  Is_Nullable," +
                                     "  Column_Default," +
                                     "  Table_Type " +
                                     "  FROM     INFORMATION_SCHEMA.COLUMNS isc " +
                                     "  INNER JOIN  information_schema.tables ist " +
                                     "              ON isc.table_name = ist.table_name " +
                                     " ORDER BY DBName,TableName,SchemaName,Ordinal_position;";
            
                myReader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
           
           
            if (myReader.HasRows)
            {                
                dataTable.Load(myReader);               
            }
            myReader.Close();
            cn.Close();

            return dataTable;

        }

        /// <summary>
        /// Функция проверяет заполненны параметры или нет
        /// </summary>
        /// <param name="databaseName">Имя базы данных</param>
        /// <param name="BaseType">ТипБазыДанных</param>
        /// <returns></returns>
        public bool VerificationInputParametr(string databaseName, string fileName)
        {
            if (databaseName == "")
            {
                resultString = "Имя базы данных не заполненно";
                return false;
            }
            if (fileName == "")
            {
                resultString = "Поле 'Файл эталонной конфигурации' не заполненно";
                return false;
            }
            return true;
        }  

        /// <summary>
        /// Подробный анализ расхождений
        /// </summary>
        /// <param name="DT_SOURCE">Эталонная структура</param> 
        /// <param name="DT_TARGET">Физическая Структура БД</param>
        public void Detail_Verification_dbSchema(DataTable DT_SOURCE,DataTable DT_TARGET)
        {
            //Выбираем название таблиц/view для сверки
            //Проверяем различия в названиях таблиц

            DataTable RDT = new DataTable("ERROR_REPORT");
            DataColumn column1 = new DataColumn("TableName", typeof(System.String));
            DataColumn column2 = new DataColumn("Column_Name", typeof(System.String));
            DataColumn column3 = new DataColumn("ERROR", typeof(System.String));
            DataColumn column4 = new DataColumn("VALUE_SOURCE", typeof(System.String));
            DataColumn column5 = new DataColumn("VALUE_TARGET", typeof(System.String));
            RDT.Columns.Add(column1);
            RDT.Columns.Add(column2);
            RDT.Columns.Add(column3);
            RDT.Columns.Add(column4);
            RDT.Columns.Add(column5);

            DataRow rowRDT;




            var distinctTableSOURCE = 
                DT_SOURCE.AsEnumerable().Select(row => new { TableName = row.Field<string>("TableName") }).Distinct(); //Выбираем уникальные имена таблиц в эталонной структуре

            var distinctTableTARGET = 
                DT_TARGET.AsEnumerable().Select(row => new { TableName = row.Field<string>("TableName") }).Distinct(); //Выбираем уникальные имена таблиц в структуре БД

            var tmpDDT = distinctTableSOURCE.Except(distinctTableTARGET);
            var tmpDDS = distinctTableTARGET.Except(distinctTableSOURCE);

            if (tmpDDT != null)
                {
                    errorString += "Имеются таблицы отсутствующие в структуре БД \n";
                
            }

            foreach (var itemERROR in tmpDDT)
                {
                    errorString += itemERROR.TableName + " отсутствует в структуре бд \n";
                    rowRDT = RDT.NewRow();
                    rowRDT["TableName"] = itemERROR.TableName;
                    rowRDT["Column_Name"] = "";
                    rowRDT["ERROR"] = "Таблица Отсутствует в структуре БД";
                    rowRDT["VALUE_SOURCE"] = "";
                    rowRDT["VALUE_TARGET"] = "";
                    RDT.Rows.Add(rowRDT);
            }

            if (tmpDDS != null)
                {
                    errorString += "Имеются таблицы отсутствующие в эталонной xml \n";
                }
            foreach (var itemERROR in tmpDDS)
                {
                    errorString += itemERROR.TableName + " отсутствует в эталонной xml \n";
                    rowRDT = RDT.NewRow();
                    rowRDT["TableName"] = itemERROR.TableName;
                    rowRDT["Column_Name"] = "";
                    rowRDT["ERROR"] = "Таблица Отсутствует в эталонной xml";
                    rowRDT["VALUE_SOURCE"] = "";
                    rowRDT["VALUE_TARGET"] = "";
                    RDT.Rows.Add(rowRDT);
            }

            //Проверяем совпадение полей и их нумерацию
            foreach (var itemS in distinctTableSOURCE)
            {
                var queryDTS =
                        from item in DT_SOURCE.AsEnumerable()
                        where item.Field<string>("TableName") == itemS.TableName
                        select new
                        {
                            TableName = item.Field<string>("TableName"),
                            Column_Name = item.Field<string>("Column_Name"),
                            Data_Type = item.Field<string>("Data_Type"),
                            Ord = item.Field<string>("Ord")
                        };
                var queryDTT =
                            from item in DT_TARGET.AsEnumerable()
                            where item.Field<string>("TableName") == itemS.TableName
                            select new
                            {
                                TableName = item.Field<string>("TableName"),
                                Column_Name = item.Field<string>("Column_Name"),
                                Data_Type = item.Field<string>("Data_Type"),
                                Ord = item.Field<string>("Ord")
                            };

                foreach (var i in queryDTS)
                    {
                    foreach (var j in queryDTT)
                    {
                        if ((i.TableName == j.TableName) && (i.Column_Name == j.Column_Name))
                        { 
                        if ( (i.Data_Type != j.Data_Type) || (i.Ord != j.Ord))
                                if (i.Data_Type != j.Data_Type)
                                {
                                    rowRDT = RDT.NewRow();
                                    rowRDT["TableName"] = i.TableName;
                                    rowRDT["Column_Name"] = i.Column_Name;
                                    rowRDT["ERROR"] = "Тип поля не совпадает";
                                    rowRDT["VALUE_SOURCE"] = i.Data_Type;
                                    rowRDT["VALUE_TARGET"] = j.Data_Type;
                                    RDT.Rows.Add(rowRDT);
                                }
                                else
                                {
                                    if (i.Ord != j.Ord)
                                    {
                                        rowRDT = RDT.NewRow();
                                        rowRDT["TableName"] = i.TableName;
                                        rowRDT["Column_Name"] = i.Column_Name;
                                        rowRDT["ERROR"] = "Порядок следования полей нарушен";
                                        rowRDT["VALUE_SOURCE"] = i.Ord;
                                        rowRDT["VALUE_TARGET"] = j.Ord;
                                        RDT.Rows.Add(rowRDT);
                                    }                                    
                                }
                            }
                        
                    }
                }

                //Проверяем наличие полей



                
                var DTS_COL =
                            from item in DT_SOURCE.AsEnumerable()
                            where item.Field<string>("TableName") == itemS.TableName
                            select new
                            {                                
                                Column_Name = item.Field<string>("Column_Name")
                            };

                var DTT_COL =
                            from item in DT_TARGET.AsEnumerable()
                            where item.Field<string>("TableName") == itemS.TableName
                            select new
                            {
                                Column_Name = item.Field<string>("Column_Name")
                            };


                var tmpDDSC = DTS_COL.Except(DTT_COL);
                var tmpDDTC = DTT_COL.Except(DTS_COL);

                foreach (var ic in tmpDDSC)
                {
                    rowRDT = RDT.NewRow();
                    rowRDT["TableName"] = itemS.TableName;
                    rowRDT["Column_Name"] = ic.Column_Name;
                    rowRDT["ERROR"] = "Поле отсутствует в структуре БД";
                    rowRDT["VALUE_SOURCE"] = "";
                    rowRDT["VALUE_TARGET"] ="";
                    RDT.Rows.Add(rowRDT);
                }
                foreach (var jc in tmpDDTC)
                {
                    rowRDT = RDT.NewRow();
                    rowRDT["TableName"] = itemS.TableName;
                    rowRDT["Column_Name"] = jc.Column_Name;
                    rowRDT["ERROR"] = "Поле отсутствует в эталонном xml";
                    rowRDT["VALUE_SOURCE"] = "";
                    rowRDT["VALUE_TARGET"] = "";
                    RDT.Rows.Add(rowRDT);
                }
            }
            RDT.AcceptChanges();
            RDT.WriteXml("detail_error_log.xml");

            string fileName_log = "log.txt";
            FileStream aFile = new FileStream(fileName_log, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(aFile);

            sw.WriteLine(errorString);
            sw.Close();
            aFile.Close();
        }

    }
}
