﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace Plugins
{
    public interface Plugin
    {
        
        String PluginName();
        DataTable ResulDT(string serverName, string databaseName);
    }
}
