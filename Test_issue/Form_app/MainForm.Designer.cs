﻿namespace Form_app
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.функцииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьЭталоннуюXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьСтруктуруБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxserverName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxdbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxfileName = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.загрузитьПлагинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.функцииToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(396, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem,
            this.toolStripMenuItem1,
            this.загрузитьПлагинToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // функцииToolStripMenuItem
            // 
            this.функцииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьЭталоннуюXMLToolStripMenuItem,
            this.проверитьСтруктуруБДToolStripMenuItem});
            this.функцииToolStripMenuItem.Name = "функцииToolStripMenuItem";
            this.функцииToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.функцииToolStripMenuItem.Text = "Функции";
            // 
            // создатьЭталоннуюXMLToolStripMenuItem
            // 
            this.создатьЭталоннуюXMLToolStripMenuItem.Name = "создатьЭталоннуюXMLToolStripMenuItem";
            this.создатьЭталоннуюXMLToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.создатьЭталоннуюXMLToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.создатьЭталоннуюXMLToolStripMenuItem.Text = "Создать Эталонную XML";
            this.создатьЭталоннуюXMLToolStripMenuItem.Click += new System.EventHandler(this.создатьЭталоннуюXMLToolStripMenuItem_Click);
            // 
            // проверитьСтруктуруБДToolStripMenuItem
            // 
            this.проверитьСтруктуруБДToolStripMenuItem.Name = "проверитьСтруктуруБДToolStripMenuItem";
            this.проверитьСтруктуруБДToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F2)));
            this.проверитьСтруктуруБДToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.проверитьСтруктуруБДToolStripMenuItem.Text = "Проверить структуру БД";
            this.проверитьСтруктуруБДToolStripMenuItem.Click += new System.EventHandler(this.проверитьСтруктуруБДToolStripMenuItem_Click);
            // 
            // textBoxserverName
            // 
            this.textBoxserverName.Location = new System.Drawing.Point(163, 28);
            this.textBoxserverName.Name = "textBoxserverName";
            this.textBoxserverName.Size = new System.Drawing.Size(208, 20);
            this.textBoxserverName.TabIndex = 3;
            this.textBoxserverName.Text = "localhost";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Сервер Базы данных:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "База Данных:";
            // 
            // textBoxdbName
            // 
            this.textBoxdbName.Location = new System.Drawing.Point(163, 54);
            this.textBoxdbName.Name = "textBoxdbName";
            this.textBoxdbName.Size = new System.Drawing.Size(208, 20);
            this.textBoxdbName.TabIndex = 5;
            this.textBoxdbName.Text = "NORTHWND";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Файл эталонной структуры:";
            // 
            // textBoxfileName
            // 
            this.textBoxfileName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxfileName.Location = new System.Drawing.Point(163, 80);
            this.textBoxfileName.Name = "textBoxfileName";
            this.textBoxfileName.Size = new System.Drawing.Size(208, 20);
            this.textBoxfileName.TabIndex = 7;
            this.textBoxfileName.Text = "real_st";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 201);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(396, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(7, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(233, 80);
            this.label4.TabIndex = 10;
            this.label4.Text = "Горячие клавиши:\r\nCTRL+N - Создание эталонной xml\r\nCTRL+F2 - Проверка структуры Б" +
    "Д\r\nCTRL+E - Выход из программы\r\nCTRL+P - Загрузка плагина";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(169, 6);
            // 
            // загрузитьПлагинToolStripMenuItem
            // 
            this.загрузитьПлагинToolStripMenuItem.Name = "загрузитьПлагинToolStripMenuItem";
            this.загрузитьПлагинToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.загрузитьПлагинToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.загрузитьПлагинToolStripMenuItem.Text = "Загрузить Плагин";
            this.загрузитьПлагинToolStripMenuItem.Click += new System.EventHandler(this.загрузитьПлагинToolStripMenuItem_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(246, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "...";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 223);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxfileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxdbName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxserverName);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Анализ структуры БД";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem функцииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьЭталоннуюXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверитьСтруктуруБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxserverName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxdbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxfileName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem загрузитьПлагинToolStripMenuItem;
        private System.Windows.Forms.Label label5;
    }
}

