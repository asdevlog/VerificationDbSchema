﻿using System;
using System.Windows.Forms;
using VarXML;
using Plugins;
using System.Reflection;
using System.Data;
namespace Form_app
{
    public partial class MainForm : Form
    {
        DataTable DT_plugin;
        
        public MainForm()
        {
            InitializeComponent();
        }

        private void создатьЭталоннуюXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Var_XML Vx = new Var_XML();
            toolStripStatusLabel1.Text = "Идет выгрузка эталлонной структуры";
            Vx.SchemaToXML(textBoxdbName.Text, textBoxfileName.Text, textBoxserverName.Text,DT_plugin);
            toolStripStatusLabel1.Text = "Выгрузка готова";
            MessageBox.Show(Vx.resultString, "Результат");
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void проверитьСтруктуруБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Идет проверка структуры БД";
            Var_XML Vx = new Var_XML();
            Vx.Verification_dbSchema(textBoxdbName.Text, textBoxfileName.Text, textBoxserverName.Text,DT_plugin);
            toolStripStatusLabel1.Text = "Проверка завершена готова";
            MessageBox.Show(Vx.resultString, "Результат");
        }

        private void загрузитьПлагинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog odf = new OpenFileDialog();
            if (odf.ShowDialog() == DialogResult.OK)
            {
                Assembly.LoadFrom(odf.FileName);
                foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface("Plugin") != null)
                        {
                            
                            Plugin pl = Activator.CreateInstance(t) as Plugin;
                            label5.Text = pl.PluginName();
                            DT_plugin = pl.ResulDT(textBoxserverName.Text,textBoxdbName.Text);
                                                        
                        }
                    }
                }

            }
        }
    }
}
