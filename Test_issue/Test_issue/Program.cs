﻿using System;
using VarXML;
using Plugins;
using System.Reflection;
using System.Data;

namespace Test_issue
{
    class Program
    {
        static void Main(string[] args)
        {
            int select_action;
            bool plugin_use = false;
            DataTable DT_plugin = null;
            string dbName="",serverName="",fileName="";
            Var_XML Vx = new Var_XML();
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Функции программы:");
                Console.WriteLine("1. Создать эталонное описание бызы данных с физической структуры БД");
                Console.WriteLine("2. Сравнение физической структуры БД с эталонным описанием");
                Console.WriteLine("3. Подключить Плагин");
                Console.WriteLine("4. Выход");
                Console.WriteLine("Выберите действие:");
                select_action = Convert.ToInt32(Console.ReadLine());
                if (select_action == 1)
                {
                    Console.Clear();
                    Console.WriteLine("Создание эталонного описания .....");
                    if (!plugin_use)
                    {
                        Console.WriteLine("Введите сервер базы данных (если сервер не указан, будет использован localhost): ");
                        serverName = Console.ReadLine();
                        Console.WriteLine("Введите название базы данных : ");
                        dbName = Console.ReadLine();
                    }
                    Console.WriteLine("Введите название файла эталонной структуры (без указания .xml) : ");
                    fileName = Console.ReadLine();


                    if (serverName == "")
                    {
                        Vx.SchemaToXML(dbName, fileName,"localhost",DT_plugin);
                    }
                    else
                    {
                        Vx.SchemaToXML(dbName, fileName, serverName, DT_plugin);
                    }


                    Console.WriteLine(Vx.resultString);
                }
                if (select_action == 2)
                {
                    Console.Clear();
                    Console.WriteLine("Сравнение эталонной конфигурации и физической модели БД.....");
                    if (!plugin_use)
                    {
                        Console.WriteLine("Введите сервер базы данных (если сервер не указан, будет использован localhost): ");
                        serverName = Console.ReadLine();
                        Console.WriteLine("Введите название базы данных : ");
                        dbName = Console.ReadLine();
                    }
                    Console.WriteLine("Введите название файла эталонной структуры (без указания .xml) : ");
                    fileName = Console.ReadLine();


                    if (serverName == "")
                    {
                        Vx.Verification_dbSchema(dbName, fileName, "localhost",DT_plugin);
                    }
                    else
                    {
                        Vx.Verification_dbSchema(dbName, fileName, serverName, DT_plugin);
                    }
                    Console.WriteLine(Vx.resultString);
                    Console.WriteLine("Сравнение физической структуры БД с эталонным описанием окончено.");




                }

                if (select_action == 3)
                {
                    Console.Clear();
                    Console.WriteLine("Подключение плагина");
                    Console.WriteLine("Введите путь до файла плагина: ");
                    string pluginName = Console.ReadLine();
                    
                    Assembly.LoadFrom(pluginName);
                    foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        foreach (Type t in a.GetTypes())
                        {
                            if (t.GetInterface("Plugin") != null)
                            {
                                Console.WriteLine("Введите сервер базы данных (если сервер не указан, будет использован localhost): ");
                                serverName = Console.ReadLine();
                                Console.WriteLine("Введите название базы данных : ");
                                dbName = Console.ReadLine();

                                plugin_use = true;
                                Plugin pl = Activator.CreateInstance(t) as Plugin;
                                Console.WriteLine(pl.PluginName());

                                DT_plugin = pl.ResulDT(serverName, dbName);

                            }
                            else
                            {
                                plugin_use = false;
                            }
                        }
                    }
                }

                if (select_action == 4)
                {
                    return;
                }


                Console.WriteLine("Нажмите любую кнопку .....");
                Console.ReadLine();
            }
                                
        }
    }
}
